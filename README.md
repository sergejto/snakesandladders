# Snakes and Ladders Kata Feature 1

<http://agilekatas.co.uk/katas/SnakesAndLadders-Kata>

## Usage examples:
Content type for all requests must be **application/json**

Start new game:
```shell
[POST] /api/game/new
body = {} 
```

Move Player 1 3 spaces:
```shell
[PUT] /api/players/0/move
body = {"steps": 3}
```

Roll dice and move Player 1:
```shell
[PUT] /api/players/0/rolldice
body = {}
```
