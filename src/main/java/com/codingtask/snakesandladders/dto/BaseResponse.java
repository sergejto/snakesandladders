package com.codingtask.snakesandladders.dto;

import com.codingtask.snakesandladders.exception.ErrorCode;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class BaseResponse<T> {
    private final T data;
    private final ErrorCode errorCode;

    public static BaseResponse successNoData() {
        return BaseResponse.builder()
                .build();
    }

    public static <T> BaseResponse<T> successWithData(T data) {
        return BaseResponse.<T>builder()
                .data(data)
                .build();
    }

    public static BaseResponse error(ErrorCode errorCode) {
        return BaseResponse.builder()
                .errorCode(errorCode)
                .build();
    }

}
