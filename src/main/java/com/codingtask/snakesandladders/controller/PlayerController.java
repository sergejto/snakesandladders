package com.codingtask.snakesandladders.controller;

import com.codingtask.snakesandladders.dto.BaseResponse;
import com.codingtask.snakesandladders.dto.PlayerMoveRequest;
import com.codingtask.snakesandladders.entity.Player;
import com.codingtask.snakesandladders.service.GameService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api/players")
@RequiredArgsConstructor
public class PlayerController {

    private final GameService gameService;

    @PutMapping(
            value = "/{playerId}/move",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<BaseResponse<Player>> movePlayer(@PathVariable Integer playerId,
                                                           @RequestBody PlayerMoveRequest playerMoveRequest) {
        return ResponseEntity.ok(BaseResponse.successWithData(gameService.movePlayer(playerId, playerMoveRequest.getSteps())));
    }

    @PutMapping(
            value = "/{playerId}/rolldice",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<BaseResponse<Player>> rollDice(@PathVariable Integer playerId) {
        return ResponseEntity.ok(BaseResponse.successWithData(gameService.rollDice(playerId)));
    }

}
