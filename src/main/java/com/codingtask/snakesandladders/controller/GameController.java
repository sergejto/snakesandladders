package com.codingtask.snakesandladders.controller;

import com.codingtask.snakesandladders.dto.BaseResponse;
import com.codingtask.snakesandladders.service.GameService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/game")
@RequiredArgsConstructor
public class GameController {
    private final GameService gameService;

    @PostMapping(value = "/new",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BaseResponse> newGame() {
        gameService.newGame(GameService.DEFAULT_BOARD_SIZE, GameService.DEFAULT_PLAYERS);
        return ResponseEntity.ok(BaseResponse.successNoData());
    }

}
