package com.codingtask.snakesandladders.controller;


import com.codingtask.snakesandladders.dto.BaseResponse;
import com.codingtask.snakesandladders.exception.ErrorCode;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionController {
    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<BaseResponse> handleEntityNotFoundException() {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(BaseResponse.error(ErrorCode.WRONG_INPUT));
    }
}
