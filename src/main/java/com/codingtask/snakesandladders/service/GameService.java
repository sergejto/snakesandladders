package com.codingtask.snakesandladders.service;

import com.codingtask.snakesandladders.entity.Board;
import com.codingtask.snakesandladders.entity.Player;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Service
@NoArgsConstructor
@Getter
@Setter
public class GameService {
    public static final int DEFAULT_BOARD_SIZE = 100;
    public static final int DEFAULT_PLAYERS = 1;
    public static final int DICE_MAX = 6;
    public static final int DICE_MIN = 1;

    private Board board;
    private List<Player> players;

    public void newGame(int boardSize, int playersCount) {
        if (boardSize <= 0 || playersCount <= 0)
            throw new IllegalArgumentException();
        this.board = new Board(boardSize);
        this.players = new ArrayList<>();
        for (int i = 1; i <= playersCount; i++) {
            this.players.add(new Player("Player " + i));
        }
    }

    public Player movePlayer(Integer playerID, int spaces) {
        if (spaces < 1 || spaces > 6 || playerID >= players.size())
            throw new IllegalArgumentException();
        Player player = players.get(playerID);
        int previousPosition = player.getPosition();
        int newPosition = previousPosition + spaces;
        if (newPosition <= board.getSize()) {
            player.setPosition(newPosition);
            if (newPosition == board.getSize()) {
                player.setWinner(true);
            }
        }
        return player;
    }

    public Player rollDice(Integer playerID) {
        int spaces = new Random().nextInt(DICE_MAX) + DICE_MIN;
        return movePlayer(playerID, spaces);
    }

    @PostConstruct
    private void postConstruct() {
        newGame(DEFAULT_BOARD_SIZE, DEFAULT_PLAYERS);
    }

}
