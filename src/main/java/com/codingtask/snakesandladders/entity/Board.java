package com.codingtask.snakesandladders.entity;

import lombok.Data;

@Data
public class Board {
    final int size;
}
