package com.codingtask.snakesandladders.entity;

import lombok.Data;

@Data
public class Player {
    public static final int START_POSITION = 1;
    private final String name;
    private int position = START_POSITION;
    private boolean winner;
}
