package com.codingtask.snakesandladders.controller;

import com.codingtask.snakesandladders.dto.PlayerMoveRequest;
import com.codingtask.snakesandladders.entity.Player;
import com.codingtask.snakesandladders.service.GameService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(value = PlayerController.class)
class PlayerControllerTest {

    private final MockMvc mockMvc;
    private final ObjectMapper objectMapper;

    @Autowired
    public PlayerControllerTest(MockMvc mockMvc, ObjectMapper objectMapper) {
        this.mockMvc = mockMvc;
        this.objectMapper = objectMapper;
    }

    @MockBean
    private GameService gameService;

    @Test
    void movePlayer() throws Exception {
        when(gameService.movePlayer(any(Integer.class), any(int.class)))
                .thenReturn(new Player("Player 1"));

        mockMvc.perform(MockMvcRequestBuilders.put("/api/players/0/move")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new PlayerMoveRequest(3))))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"data\":{\"name\":\"Player 1\",\"position\":1,\"winner\":false},\"errorCode\":null}"));
    }

    @Test
    void rollDice() throws Exception {
        when(gameService.rollDice(any(Integer.class)))
                .thenReturn(new Player("Player 1"));

        mockMvc.perform(MockMvcRequestBuilders.put("/api/players/0/rolldice")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{}"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"data\":{\"name\":\"Player 1\",\"position\":1,\"winner\":false},\"errorCode\":null}"));
    }
}
