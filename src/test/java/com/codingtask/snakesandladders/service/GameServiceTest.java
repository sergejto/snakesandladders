package com.codingtask.snakesandladders.service;

import com.codingtask.snakesandladders.entity.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;

class GameServiceTest {

    private final GameService gameService = new GameService();
    private static final int BOARD_SIZE = 100;
    public static final int PLAYERS_COUNT = 1;

    @BeforeEach
    void init() {
        gameService.newGame(BOARD_SIZE, PLAYERS_COUNT);
    }

    @Test
    void newGame() {
        Player player = new Player("Player 1");
        assertEquals(BOARD_SIZE, gameService.getBoard().getSize());
        assertEquals(PLAYERS_COUNT, gameService.getPlayers().size());
        assertEquals(player, gameService.getPlayers().get(0));
    }

    @ParameterizedTest
    @MethodSource("allDiceValues")
    void movePlayerSuccessful(int spaces) {
        gameService.movePlayer(0, spaces);
        assertEquals(Player.START_POSITION + spaces, gameService.getPlayers().get(0).getPosition());
    }

    @Test
    void movePlayerErrorDiceOutOfBounds() {
        assertThrows(IllegalArgumentException.class, () -> gameService.movePlayer(0, GameService.DICE_MIN - 1));
        assertThrows(IllegalArgumentException.class, () -> gameService.movePlayer(0, GameService.DICE_MAX + 1));
    }

    @Test
    void movePlayerErrorPlayerIdOutOfBounds() {
        assertThrows(IllegalArgumentException.class, () -> gameService.movePlayer(PLAYERS_COUNT, GameService.DICE_MIN));
    }

    @ParameterizedTest
    @MethodSource("allDiceValues")
    void movePlayerNotPossibleAndNotWinTheGame(int spaces) {
        int startPosition = BOARD_SIZE - spaces + 1;
        gameService.getPlayers().get(0).setPosition(startPosition);
        gameService.movePlayer(0, spaces);
        assertEquals(startPosition, gameService.getPlayers().get(0).getPosition());
        assertFalse(gameService.getPlayers().get(0).isWinner());
    }

    @ParameterizedTest
    @MethodSource("allDiceValues")
    void movePlayerAndWinTheGame(int spaces) {
        int startPosition = BOARD_SIZE - spaces;
        gameService.getPlayers().get(0).setPosition(startPosition);
        gameService.movePlayer(0, spaces);
        assertEquals(startPosition + spaces, gameService.getPlayers().get(0).getPosition());
        assertTrue(gameService.getPlayers().get(0).isWinner());
    }

    @Test
    void rollDice() {
        for (int i = 0; i < BOARD_SIZE / GameService.DICE_MAX; i++) {
            int lastPosition = gameService.getPlayers().get(0).getPosition();
            gameService.rollDice(0);
            int distance = gameService.getPlayers().get(0).getPosition() - lastPosition;
            assertTrue(GameService.DICE_MIN <= distance && distance <= GameService.DICE_MAX);
        }
    }

    private static IntStream allDiceValues() {
        return IntStream.range(GameService.DICE_MIN, GameService.DICE_MAX + 1);
    }
}
